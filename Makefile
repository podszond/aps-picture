SHELL = /bin/sh
CC = gcc -g -Wall -Werror
LDLIBS = -lm

main: main.c

valgrind: main
	valgrind --tool=cachegrind --cachegrind-out-file=$<.out \
	    --I1=32768,8,64 --D1=32768,8,64 --LL=1048576,16,64 ./$< normal.ppm
	cg_annotate $<.out $(realpath $<.c)

clean:
	$(RM) main *.out output.*
