#include <stdio.h>
#include <time.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>

#define NORTH -1
#define WEST -1
#define SOUTH -1
#define EAST -1
#define MID 5 
#define NORTHEAST 0
#define NORTHWEST 0
#define SOUTHEAST 0
#define SOUTHWEST 0

int main(int argc, char **argv) {
    FILE * input = NULL, * output = NULL, * histogram = NULL;
    int width = 0, height = 0, max = 0, i, j, interval1 = 0, interval2 = 0,
            interval3 = 0, interval4 = 0, interval5 = 0, blackwhite = 0, red, green, blue;
    char * header = "P6";
    unsigned char * arr = NULL, *arr_out = NULL;

    if (argc < 2) {
        return 1;
    }

    if (!(input = fopen(argv[1], "rb"))) {
        return 1;
    }
    if (!(output = fopen("output.ppm", "wb"))) {
        return 1;
    }
    if (!(histogram = fopen("output.txt", "w"))) {
        return 1;
    }

    fseek(input, 3, SEEK_SET);

    if (!fscanf(input, " %d %d\n%d\n", &width, &height, &max)) {
        return 1;
    }

    fprintf(output, "%s\n%d\n%d\n%d\n", header, width, height, max);


    arr = malloc(sizeof (unsigned char) * 3 * height * width);
    arr_out = malloc(sizeof (unsigned char) * 3 * height * width);

    if (!fread(arr, sizeof (unsigned char), height * width * 3, input))
        return 1;

    for (i = 0; i < height; i++) {
        for (j = 0; j < (width * 3); j += 3) {
            if ((j == 0) || (j == width * 3 - 3) || (i == 0) || (i == height - 1)) {
                red = arr[i * 3 * width + j];
                green = arr[i * 3 * width + j + 1];
                blue = arr[i * 3 * width + j + 2];
            } else {
                red = (MID * arr[i * 3 * width + j])
                        + (NORTH * arr[(i - 1) * 3 * width + j])
                        + (EAST * arr[i * 3 * width + j + 3])
                        + (WEST * arr[i * 3 * width + j - 3])
                        + (SOUTH * arr[(i + 1) * 3 * width + j])
                        /*
                        + (NORTHWEST * arr[i - 1][j - 3])
                        + (NORTHEAST * arr[i - 1][j + 3])
                        + (SOUTHWEST * arr[i + 1][j - 3])
                        + (SOUTHEAST * arr[i + 1][j + 3])
                         */;

                green = (MID * arr[i * 3 * width + j + 1])
                        + (NORTH * arr[(i - 1) * 3 * width + j + 1])
                        + (EAST * arr[i * 3 * width + j + 4])
                        + (WEST * arr[i * 3 * width + j - 2])
                        + (SOUTH * arr[(i + 1) * 3 * width + j + 1])
                        /*
                        + (NORTHWEST * arr[i - 1][j - 2])
                        + (NORTHEAST * arr[i - 1][j + 4])
                        + (SOUTHWEST * arr[i + 1][j - 2])
                        + (SOUTHEAST * arr[i + 1][j + 4])
                         */;

                blue = (MID * arr[i * 3 * width + j + 2])
                        + (NORTH * arr[(i - 1) * 3 * width + j + 2])
                        + (EAST * arr[i * 3 * width + j + 5])
                        + (WEST * arr[i * 3 * width + j - 1])
                        + (SOUTH * arr[(i + 1) * 3 * width + j + 2])
                        /*
                        + (NORTHWEST * arr[i - 1][j - 1])
                        + (NORTHEAST * arr[i - 1][j + 5])
                        + (SOUTHWEST * arr[i + 1][j - 1])
                        + (SOUTHEAST * arr[i + 1][j + 5])
                         */;

            }

            if (red > max) {
                red = max;
            } else if (red < 0) {
                red = 0;
            }

            if (green > max) {
                green = max;
            } else if (green < 0) {
                green = 0;
            }

            if (blue > max) {
                blue = max;
            }
            else if (blue < 0) {
                blue = 0;
            }

            arr_out[i * 3 * width + j] = (unsigned char) red;
            arr_out[i * 3 * width + j + 1] = (unsigned char) green;
            arr_out[i * 3 * width + j + 2] = (unsigned char) blue;

            blackwhite = round(0.2126 * red + 0.7152 * green + 0.0722 * blue);


            if (blackwhite <= 50) {
                interval1++;
            } else if (blackwhite <= 101) {
                interval2++;
            } else if (blackwhite <= 152) {
                interval3++;
            } else if (blackwhite <= 203) {
                interval4++;
            } else {
                interval5++;
            }
        }
    }

    fprintf(histogram, "%d %d %d %d %d", interval1, interval2, interval3, interval4, interval5);

    fwrite(arr_out, sizeof (unsigned char),height * width * 3, output);
    fclose(input);
    fclose(output);
    fclose(histogram);
    free(arr);
    free(arr_out);

    return 0;
}
